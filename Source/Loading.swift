//
//  Loading.swift
//  
//
//  Created by Daniel Rutkovsky
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import RxSwift

public extension Observable {
    func handleLoading(loadingView: LoadingViewProtocol?,
                       loadingMessage: String? = nil) -> Observable<Element> {
        return self.do(onNext: { (_) in
            loadingView?.hideLoading()
        }, onError: { (_) in
            loadingView?.hideLoading()
        }, onCompleted: {
            loadingView?.hideLoading()
        }, onSubscribe: {
            loadingView?.showLoading(message: loadingMessage)
        }, onDispose: {
            loadingView?.hideLoading()
        })
    }
}

public extension PrimitiveSequence where Trait == CompletableTrait, Element == Never {
    func handleLoading(loadingView: LoadingViewProtocol?,
                       loadingMessage: String? = nil) -> PrimitiveSequence<Trait, Element> {
        return self.do(onError: { (_) in
            loadingView?.hideLoading()
        },
                       onCompleted: {
                        loadingView?.hideLoading()
        },
                       onSubscribe: {
                        loadingView?.showLoading(message: loadingMessage)
        },
                       onDispose: {
                        loadingView?.hideLoading()
        })
    }
}

public extension PrimitiveSequence where Trait == SingleTrait {
    func handleLoading(loadingView: LoadingViewProtocol?,
                       loadingMessage: String? = nil) -> PrimitiveSequence<Trait, Element> {
        return self.do(onSuccess: nil,
                       onError: { (_) in
                        loadingView?.hideLoading()
        },
                       onSubscribe: {
                        loadingView?.showLoading(message: loadingMessage)
        },
                       onDispose: {
                        loadingView?.hideLoading()
        })
    }
}

public extension PrimitiveSequence where Trait == MaybeTrait {
    func handleLoading(loadingView: LoadingViewProtocol?,
                       loadingMessage: String? = nil) -> PrimitiveSequence<Trait, Element> {
        return self.do(onError: { (_) in
            loadingView?.hideLoading()
        },
                       onCompleted: {
                        loadingView?.hideLoading()
        },
                       onSubscribe: {
                        loadingView?.showLoading(message: loadingMessage)
        },
                       onDispose: {
                        loadingView?.hideLoading()
        })
    }
}
