//
//  BaseRouter.swift
//
//
//  Created by Daniel Rutkovsky on 3/6/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import UIKit

/// BaseRouter
///
/// A default implementation of a BaseRouter class that conforms to a BaseRouterProtocol protocol.
/// It's designed to reduce a boiler plate code that will be present in each router class.
/// It only provides a storage for `navigationController`, `onCancel` and `onFinish` arguments that
/// are passed when the routes is started.
/// 
/// This class does NOT contain any logic.
///
open class BaseRouter: BaseRouterProtocol {
    public var navigationController: UINavigationController?
    public var onCancel:  (() -> Void)?
    public var onFinish:  (() -> Void)?

    /// A public init method
    public init() {
    }

    /// Implementation a start method from BaseRouterProtocol
    /// This implemntation should be overriden by a child router. It only stores the passed arguments.
    ///
    /// - Parameters:
    ///   - navigationController: controller on which a viewControllers can be preseneted
    ///   - onCancel: callback that is called when the desired function of the router was not finihed
    ///   - onFinish: callback that is called when the desired function of the router was finihed
    ///
    /// - SeeAlso: `BaseRouterProtocol.start()`
    ///
    open func start(navigationController: UINavigationController,
                    onCancel: @escaping () -> Void,
                    onFinish: @escaping () -> Void) {
        self.navigationController = navigationController
        self.onCancel = onCancel
        self.onFinish = onFinish
    }

    /// Method that starts a new router with existing navigation controller
    ///
    /// - Parameters:
    ///   - router: A router to be started
    ///   - onCancel: callback that is called when the desired function of the router was not finihed
    ///   - onFinish: callback that is called when the desired function of the router was finihed
    public func startRouter(router: BaseRouter,
                            onCancel: @escaping () -> Void,
                            onFinish: @escaping () -> Void) {
        let screen = router
        if let navigationController = navigationController {
            screen.start(navigationController: navigationController,
                         onCancel: onCancel,
                         onFinish: onFinish)
        }
    }
}

open class BaseRouterWithDependencies<T>: BaseRouter {
    public let dependencies: T
    public init(dependencies: T) {
        self.dependencies = dependencies
        super.init()
    }
}
