//
//  BaseRouterProtocol.swift
//
//
//  Created by Daniel Rutkovsky on 3/15/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import UIKit

/// BaseRouterProtocol
///
/// A Protocol that provides a basic contract for router based navigation.
/// Each and every router can be started from it's uknown 👽 parent. 
/// After the router has done it's job  -> it either calls 
/// ❌`onCancel` or
/// ✅`onFinish` 
/// depending on wheter the job was successfull or not
///
///     Example:
///     1) A login router is started.
///     2) It's check wheter the user is already logged in (maybe using presenter)
///         2a) User is loggied in -> `onFinish` is called and user account provided ✅
///         2b) User is not logged in -> login router's work continues
///     3) Login screen is presented
///     3a) User fills in username/password -> `onFinish` is called and user account provided ✅
///     3b) User cancels login (register new account, anonymous, ...) 
///             -> `onCancel` is called and parent router takes the action ❌
///
public protocol BaseRouterProtocol {
    /// A function that all routers have to implement.
    /// It provides the desired functionality of a router.
    ///
    /// - Parameters:
    ///   - navigationController: controller on which a viewControllers can be preseneted
    ///   - onCancel: callback that is called when the desired function of the router was not finihed
    ///   - onFinish: callback that is called when the desired function of the router was finihed
    func start(navigationController: UINavigationController,
               onCancel: @escaping () -> Void,
               onFinish: @escaping () -> Void)
}
