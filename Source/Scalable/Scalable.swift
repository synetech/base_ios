//
//  Scalable.swift
//
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import Device
import UIKit

/// Scalable
///
/// A Protocol that enables constraint scaling on different device sizes
///
/// Default implementation will scale the size down for 4'' iPhone and scale up for 5.5'' iPhone.
public protocol Scalable {
    var scaled: CGFloat { get }
}

extension Scalable {
    fileprivate func calculate(_ original: CGFloat) -> CGFloat {
        switch Device.size() {
        case .screen3_5Inch:
            return original * CGFloat(3.5 / 4.7)
        case .screen4Inch:
            return original * CGFloat(4 / 4.7)
        case .screen5_5Inch:
            return original * CGFloat(5.5 / 4.7)
        case .screen5_8Inch:
            return original * CGFloat(5.8 / 4.7)
        case .screen6_1Inch:
            return original * CGFloat(6.1 / 4.7)
        case .screen6_5Inch:
            return original * CGFloat(6.5 / 4.7)
        default:
            return original
        }
    }
}

extension Int: Scalable {
    public var scaled: CGFloat {
        return calculate(CGFloat(self))
    }
}

extension CGFloat: Scalable {
    public var scaled: CGFloat {
        return calculate(CGFloat(self))
    }
}

extension Double: Scalable {
    public var scaled: CGFloat {
        return calculate(CGFloat(self))
    }
}
