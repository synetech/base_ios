//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import UIKit

public extension UIStackView {

    @discardableResult
    func addArrangedSubview(_ views: UIView...) -> UIStackView {
        views.forEach { self.addArrangedSubview($0) }
        return self
    }

    @discardableResult
    func addArrangedSubviewArray(_ views: [UIView]) -> UIStackView {
        views.forEach { self.addArrangedSubview($0) }
        return self
    }
}
