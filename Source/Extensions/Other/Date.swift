//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import Foundation

public extension Date {
    var formatedStringDDMMYYYY: String {
        return DateFormatter.localizedString(from: self,
                                             dateStyle: .short,
                                             timeStyle: .none)
    }
}
