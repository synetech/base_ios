//
//  UIViewController.swift
//  Pods
//
//  Created by Daniel Rutkovsky on 10/27/17.
//

import UIKit

/// Some safe area and keyboard extension which should be called on UIViewController
public extension UIViewController {

    /// Aligns the view to the top of safe area (not the window!)
    /// Very useful for X iPhones, where is critical to not place views outside the top of safe area (notch)
    ///
    /// - Parameters:
    ///   - viewToAnchor: view to be aligned to the safe area edge
    ///   - offset: distance from the safe area edge (optional, default is 0)
    public func anchorToTop(_ viewToAnchor: UIView, offset: CGFloat = 0) {
        if #available(iOS 11, *) {
            let guide = self.view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                guide.topAnchor.constraint(equalTo: viewToAnchor.topAnchor, constant: offset)
                ])
        } else {
            NSLayoutConstraint.activate([
                self.topLayoutGuide.bottomAnchor.constraint(equalTo: viewToAnchor.topAnchor, constant: offset)
                ])
        }
    }

    /// Aligns the view to the bottom of safe area (not the window!)
    /// Very useful for X iPhones, where is critical to not place views outside the bottom of safe area (navigation line)
    ///
    /// - Parameters:
    ///   - viewToAnchor: view to be aligned to the safe area edge
    ///   - offset: distance from the safe area edge (optional, default is 0)
    public func anchorToBottom(_ viewToAnchor: UIView, offset: CGFloat = 0) {
        if #available(iOS 11, *) {
            let guide = self.view.safeAreaLayoutGuide
            NSLayoutConstraint.activate([
                guide.bottomAnchor.constraint(equalTo: viewToAnchor.bottomAnchor, constant: offset)
                ])
        } else {
            NSLayoutConstraint.activate([
                self.bottomLayoutGuide.topAnchor.constraint(equalTo: viewToAnchor.bottomAnchor, constant: offset)
                ])
        }
    }

    /// Hides keyboard when user taps outside of it
    public func hideKeyboardWhenTappedAround() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    // Keyboard is dismissed and editing mode ends
    @objc
    public func dismissKeyboard() {
        view.endEditing(true)
    }
}
