//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import Foundation

public extension String {
    /// A variable that returns localized variant of the current string
    ///
    /// - Precondition: A string has to be defined in _Localized.strings_ to be translated
    ///
    /// Implementation:
    ///
    ///     `"test".localied` will return instance of `NSLocalizedString("test", tableName: nil,
    ///                                                bundle: Bundle.main, value: "", comment: "")`
    ///
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }

    /// Return the first letter of this string
    var firstLetter: String {
        return String(self.prefix(1))
    }

    /// Return a string combined from first letters of each word of this string
    var initials: String {
        return self.components(separatedBy: " ")
            .map({ (string) in
                return String(string.prefix(1))
            })
            .reduce("") { (resultString, firstLetter) in
                return resultString.appending(firstLetter)
            }
    }
}
