//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import UIKit

public extension UIImageView {
    func keepAspectRatio() {
        keepAspectRatioHeightBasedOnWidth()
    }

    func keepAspectRatioHeightBasedOnWidth() {
        if let image = self.image {
            let ratio: CGFloat = image.size.height / image.size.width
            self.addConstraint(NSLayoutConstraint(item: self,
                                                  attribute: .height,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: .width,
                                                  multiplier: ratio,
                                                  constant: 0)
            )
        }
    }

    func keepAspectRatioWidthBasedOnHeight() {
        if let image = self.image {
            let ratio: CGFloat = image.size.width / image.size.height
            self.addConstraint(NSLayoutConstraint(item: self,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: self,
                                                  attribute: .height,
                                                  multiplier: ratio,
                                                  constant: 0)
            )
        }
    }
}
