//
//  BackgroundWorker.swift
//  BaseProject
//
//  Created by Jana Ernekerova on 03/11/2017.
//  Copyright © 2017 SYNETECH s.r.o. All rights reserved.
//

import Foundation
import RxSwift

/// Makes sure your RxStream is on background thread not blocking UIThread
///
/// Example usage:
/// let background = BackgroundWorker()
/// dependencies.databaseStorage
///     .getAll(Login_UserAccountstDBEntity.self)
///     .subscribeOn(background)
///     .observeOn(background)...
final class BackgroundWorker: NSObject {
    fileprivate var thread: Thread!
    private var block: (() -> Void)!

    internal func runBlock() { block() }

    internal func start(_ block: @escaping () -> Void) {
        self.block = block

        let threadName = """
        \(String(describing: self).components(separatedBy: .punctuationCharacters)[1])-\(UUID().uuidString)
        """

        if #available(iOS 10.0, *) {
            thread = Thread { [weak self] in
                self?.doWork()
            }
        } else {
            thread = Thread.init(target: self, selector: #selector(doWork), object: nil)
        }
        thread.name = threadName
        thread.start()

        if #available(iOS 10.0, *) {
            RunLoop.current.perform {
                block()
            }
        } else {
            RunLoop.current.perform(#selector(executeBlock),
                                    on: thread,
                                    with: nil,
                                    waitUntilDone: true)
        }
    }

    @objc func executeBlock() {
        self.block()
    }

    @objc func doWork() {
        while !self.thread.isCancelled {
            RunLoop.current.run(
                mode: RunLoop.Mode.default,
                before: Date().addingTimeInterval(0.1))
        }
        Thread.exit()
    }

    public func stop() {
        thread.cancel()
    }
}

extension BackgroundWorker: ImmediateSchedulerType {
    func schedule<StateType>(_ state: StateType, action: @escaping (StateType) -> Disposable) -> Disposable {
        let disposable = SingleAssignmentDisposable()

        self.start {
            if disposable.isDisposed {
                self.stop()
                return
            }
            disposable.setDisposable(action(state))
            if disposable.isDisposed {
                self.stop()
                return
            }
        }

        return disposable
    }
}
