//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

public extension CGFloat {
    /// A listener for keyboard changes implemneted via RxSwift
    ///
    /// - Returns: Observable CGFloat that will call onNext everytime a keyboard height changes.
    static func keyboardHeight() -> Observable<CGFloat> {
        return Observable
            .from([
                NotificationCenter
                    .default
                    .rx
                    .notification(UIResponder.keyboardWillChangeFrameNotification)
                    .map { notification -> CGFloat in
                        (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                    },
                NotificationCenter
                    .default
                    .rx
                    .notification(UIResponder.keyboardWillHideNotification)
                    .map { _ -> CGFloat in
                        0
                    }
                ])
            .merge()
    }
}
