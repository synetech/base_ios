//
//  Extensions.swift
//  
//
//  Created by Daniel Rutkovsky on 9/6/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

public extension UIView {

    func fadeTransition(_ duration: CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: convertFromCATransitionType(CATransitionType.fade))
    }

    /// Returns number of points from the origin top to this views bottom (origin.y + height)
    var bottom: CGFloat {
        return self.frame.origin.y + self.frame.height
    }

    /// Returns number of points from the origin top to this views top
    var top: CGFloat {
        return self.frame.origin.y
    }

    /// Returns number of points from the origin top to this views left
    var left: CGFloat {
        return self.frame.origin.x
    }

    /// Returns number of points from the origin top to this views right (origin.x + width)
    var right: CGFloat {
        return self.frame.origin.x + self.frame.width
    }
}

extension Reactive where Base: UIView {

    var tap: ControlEvent<UITapGestureRecognizer> {
        let tapGesture = UITapGestureRecognizer()
        base.addGestureRecognizer(tapGesture)
        return tapGesture.rx.event
    }

    func tap(callback: @escaping () -> Void) -> Disposable {
        return self.tap
            .subscribe(onNext: { (_) in
                callback()
            })
    }
}
// Helper function inserted by Swift 4.2 migrator.
private func convertFromCATransitionType(_ input: CATransitionType) -> String {
	return input.rawValue
}
