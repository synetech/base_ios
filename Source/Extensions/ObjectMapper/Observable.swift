//
//  Observable.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 5/19/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import RxSwift

public extension Observable where Element: Moya.Response {
    /// Map Moya Response into a Object using Object Mapper
    ///
    /// - Precondition: The Object must extend Mappable protocol
    /// - Returns: Observable of the mapped object
    func mapJSON<T: Mappable>() -> Observable<T> {
        return map({ (element) -> T in
            if let json = try? element.mapString(), let object = Mapper<T>().map(JSONString: json) {
                return object
            }
            throw MoyaError.jsonMapping(element)
        })
    }
}
