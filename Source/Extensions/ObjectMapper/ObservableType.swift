//
//  ObservableType.swift
//  
//
//  Created by Daniel Rutkovsky
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import Moya
import ObjectMapper
import RxSwift

public extension ObservableType {
    func asCompletable() -> Completable {
        return Completable.create { (completable) -> Disposable in
            let disposable = self
                .take(1)
                .subscribe({ (event) in
                    switch event {
                    case .completed:
                        completable(.completed)
                    case .next:
                        completable(.completed)
                    case .error(let error):
                        completable(.error(error))
                    }
                })
            return Disposables.create {
                disposable.dispose()
            }
        }
    }
}

public extension Observable where Element: Moya.Response {
    /// Map Moya Response into a Object using Object Mapper
    ///
    /// - Precondition: The Object must extend Mappable protocol
    /// - Returns: Observable of the mapped object
    func mapJSON<T: Mappable>(type: T.Type) -> Observable<T> {
        return mapJSON()
    }
}
