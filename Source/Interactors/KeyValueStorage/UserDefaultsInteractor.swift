//
//  UserDefaultsInteractor.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 4/7/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

/// Enum (list) of known keys that can be used to store values in user defaults key-value data store
///
/// - onboardingShown: was the onboarding shown? -> should it be shown again?
//public enum UserDefaultsInteractorKeys {
//    case onboardingShown
//    case isFirstRun
//    case networkingCache(endpoint: String)
//
//    var rawValue: String {
//        switch self {
//        case .isFirstRun:
//            return "isFirstRun"
//        case .onboardingShown:
//            return "onboardingShown"
//        case .networkingCache(let endpoint):
//            return "networkingCache_\(endpoint)"
//        }
//    }
//}

public protocol StringRepresentable {
    var rawValue: String { get }
}

/// A contract that a defaults (key-value) interactor has
/// It's designed to store simple values for given keys
public protocol DefaultsInteractor {
    associatedtype Keys: StringRepresentable
    /// A generic save method that will take any object that can be saved into the key-value store and stores it
    ///
    /// - Parameters:
    ///   - object: object to be stored
    ///   - key: key under which will be the object stored
    ///
   func save<T>(object: T, key: Keys)

    /// A generic get method to retrieve a stored values in user defaults (key-value) store if exists
    ///
    /// - Parameter key: key under which is the object stored
    /// - Returns: object or nil
    func get<T>(key: Keys) -> T?

    /// A generic get method to retrieve a stored values in user defaults (key-value) store if exists
    /// This method will return the stored object or a default value
    ///
    /// - Parameters:
    ///   - key: key under which is the object stored
    ///   - defaultValue: default value that will be returned if the object does not exist in the store
    /// - Returns: object or default value
    func get<T>(key: Keys, defaultValue: T) -> T

    /// A generic remove to remove the object from the storage
    ///
    /// - Parameter key: key under which is the object stored
    func remove(key: Keys)
}

public extension DefaultsInteractor {
    public func save<T>(object: T, key: Keys) {
        UserDefaults.standard.set(object, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }

    public func get<T>(key: Keys) -> T? {
        return UserDefaults.standard.object(forKey: key.rawValue) as? T
    }

    public func get<T>(key: Keys, defaultValue: T) -> T {
        if let object: T = get(key: key) {
            return object
        }
        return defaultValue
    }

    public func remove(key: Keys) {
        UserDefaults.standard.removeObject(forKey: key.rawValue)
    }
}
