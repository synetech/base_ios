//
//  HasDatabaseStorage.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 7/24/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

/// HasDatabaseStorage
///
/// A protocol that is used to provide database storage by an dependecy injection
public protocol HasDatabaseStorage {
    var databaseStorage: DatabaseStorage { get }
}
