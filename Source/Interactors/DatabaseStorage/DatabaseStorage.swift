//
//  DatabaseStorage.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 5/30/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift

/// A database storage protocol that combines the ability to store sequecnies or just one object
/// - Warning: This protocol is designed to work only wiht Realm databse
public protocol DatabaseStorage: DatabaseStorageOne, DatabaseStorageMany {
    func deleteDatabase()
}

/// A database storage protocol that describes how to work with one object
/// - Warning: This protocol is designed to work only wiht Realm databse
public protocol DatabaseStorageOne {
    typealias DatabaseObject = Object
    /// Add any object into the database
    ///
    /// - Note: The database will try to update objects that have primary key
    ///
    /// - Parameter object: Object to be added
    /// - Returns: Observable added object -> when the object changes a new will be sent
    func add<T>(_ object: T) -> Observable<T> where T: DatabaseObject
    
    /// Delete object from database
    ///
    /// - Parameter object: object to be deleted
    /// - Returns: Completable to recive notification that the task is done
    func delete<T>(_ object: T) -> Completable where T: DatabaseObject
    
    ///  Get item from database by id as Single
    ///
    /// - Parameters:
    ///   - id: id of the item
    ///   - type: type of the item
    /// - Returns: Single item returned from database
    func get<T, U>(id: U, type: T.Type) -> Single<T> where T: DatabaseObject
    
    ///  Get item from database by id
    ///
    /// - Parameters:
    ///   - id: id of the item
    ///   - type: type of the item
    /// - Returns: Observable item returned from database
    func get<T, U>(id: U, type: T.Type) -> Observable<T> where T: DatabaseObject
    
    ///  Get first item from database
    ///
    /// - Parameters:
    ///   - type: type of the item
    /// - Returns: Observable item returned from database
    func getFirst<T>(_ type: T.Type) -> Observable<T> where T: DatabaseObject
    
    /// Update item in database
    ///
    /// - Parameters:
    ///     - object: Item to with updates
    ///     - writeBlock: Block that provides a way to update given object in realm
    /// - Returns: Observable added object -> when the object changes a new will be sent
    func update<T>(_ object: T, writeBlock: @escaping (_ writableObject: T) -> ()) -> Completable where T: DatabaseObject
}

/// A database storage protocol that describes how to work with multiple objects
public protocol DatabaseStorageMany {
    typealias DatabaseObject = Object
    /// Add any objects into the database
    ///
    /// - Note: The database will try to update objects that have primary key
    /// - Parameter objects: Objects to be added
    /// - Returns: Observable added objects -> when the objects changes a new will be sent
    func addAll<T: Sequence>(_ objects: T) -> Observable<T> where T.Iterator.Element: DatabaseObject
    
    /// Delete objects from database
    ///
    /// - Parameter object: objects to be deleted
    /// - Returns: Completable to recive notification that the task is done
    func deleteAll<T: Sequence>(_ objects: T) -> Completable where T.Iterator.Element: DatabaseObject
    
    /// Delete objects from database
    ///
    /// - Parameter object: objects to be deleted
    /// - Returns: Completable to recive notification that the task is done
    func deleteAll<T>(_ objects: T.Type) -> Completable where T: DatabaseObject
    
    ///  Get items from database
    ///
    /// - Parameters:
    ///   - type: type of the items
    /// - Returns: Observable items returned from database
    func getAll<T>(_ type: T.Type) -> Observable<Results<T>> where T: DatabaseObject
    
    /// Get items from database filtered by predicate
    ///
    /// When the predicate chagnes the results will update accordingally
    ///
    /// - Parameters:
    ///   - predicate: filter predicate
    ///   - type: type of the items
    /// - Returns: Observable items returned from database
    func getWithPredicate<T>(predicate: Observable<NSPredicate?>,
                             type: T.Type) -> Observable<Results<T>> where T: DatabaseObject
    
    /// Update multiple objects in database in one transaction
    ///
    /// - Parameters:
    ///   - objects: objects that should be updated
    ///   - writeBlock: callback in which you update the objects during the transaction
    /// - Returns: Completable that indicates state of the transaction
    func updateAll<T>(_ objects: T,
                      writeBlock: @escaping (T) -> ())
        -> Completable where T: Sequence, T.Element: DatabaseStorageOne.DatabaseObject
    
    /// Update multiple objects in database in one transaction one by one
    ///
    /// - Parameters:
    ///   - objects: objects that should be updated
    ///   - writeBlock: callback in which you update the objects during the transaction one by one
    /// - Returns: Completable that indicates state of the transaction
    func updateAll<T, U>(_ objects: T,
                         writeBlockForEachElement: @escaping (T.Element) -> ())
        -> Completable where T: Sequence, U == T.Element, U: DatabaseStorageOne.DatabaseObject
    
    /// First delete current objects and then add any objects in to the database (all the same type)
    ///
    /// - Parameter objects: Objects to be added
    /// - Returns: Observable added objects -> when the objects changes a new will be sent
    func deleteCurrentAndInsertAll<T: Sequence>(_ objects: T) -> Observable<T> where T.Iterator.Element: DatabaseObject
}
