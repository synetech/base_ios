//
//  DatabaseDeletable.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 6/12/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import RealmSwift

/// A protocol that enables us to delete Realm obejct recursivelly
@objc public protocol DatabaseDeletable {
    /// Delete method -> this will delete the object from database
    /// - Precondition: This has to be called during realm write transaction
    @objc
    func delete()
    /// A prepare method. This method is called right before delete method -> Delete child objects in this method.
    @objc
    func prepareDelete()
}

extension Object: DatabaseDeletable {
    @objc
    open func prepareDelete() {
        // Nothing to do here
    }

    @objc
    open func delete() {
        prepareDelete()
        if let realm = realm, realm.isInWriteTransaction {
            realm.delete(self)
        } else {
            fatalError("This method has to be called on a realm object in realm write transaction!")
        }
    }
}
