//
//  LiveData.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 6/14/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import RealmSwift

/// LiveData
///
/// A class that provides the ability to wrap any data in a lazy way 
/// You just need to init this class with a callback for the data, therefore the data are not accest unless 
/// someone accesses them. This is useful when using Realm, as the results can be converted into the LiveData 
/// put insidet a table view, however only cells that are visible will be loaded from the database.
public class LiveData<T> {
    private let objectReferenceCallback: () -> T
    private var objectReferenceCache: T?
    public var data: T {
        return getCachedValue(callback: objectReferenceCallback)
    }

    public init(_ dataCallback: @escaping () -> T) {
        objectReferenceCallback = dataCallback
    }

    public init(data: T) {
        objectReferenceCallback = { data }
        objectReferenceCache = data
    }

    private func getCachedValue(callback: () -> T) -> T {
        if let objectReferenceCache = objectReferenceCache {
            if let realmObject = objectReferenceCache as? Object,
                realmObject.isInvalidated {
                    return callback()
            }
            return objectReferenceCache
        } else {
            let data = callback()
            objectReferenceCache = data
            return data
        }
    }
}
