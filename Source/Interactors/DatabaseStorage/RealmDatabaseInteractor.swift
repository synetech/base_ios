//
//  RealmDatabaseInteractor.swift
//  SYNBase
//
//  Created by Daniel Rutkovsky on 9/7/17.
//

import Foundation
import RealmSwift
import RxSwift

public struct RealmDatabaseInteractor: DatabaseStorage {
    public typealias DatabaseObject = Object
    
    fileprivate let databaseSchemaVersion: UInt64
    fileprivate let migrationBlock: RealmSwift.MigrationBlock?
    fileprivate static let config = { (databaseSchemaVersion: UInt64,
        migrationBlock: RealmSwift.MigrationBlock?) -> Realm.Configuration in
        var config = Realm.Configuration()
        config.schemaVersion = databaseSchemaVersion
        if let migrationBlock = migrationBlock {
            config.migrationBlock = migrationBlock
        }
        config.deleteRealmIfMigrationNeeded = migrationBlock == nil
        #if DEBUG
        print("[DEBUG] Realm location is \(config.fileURL?.absoluteString ?? "File not found")")
        #endif
        return config
    }
    
    public init(databaseSchemaVersion: UInt64, migrationBlock: RealmSwift.MigrationBlock? = nil) {
        self.databaseSchemaVersion = databaseSchemaVersion
        self.migrationBlock = migrationBlock
    }
    
    public func deleteDatabase() {
        writeOperation(operation: { (realm) in
            realm.deleteAll()
        }, onError: { (_) in
            fatalError("Could not delete database")
        })
    }
}

extension RealmDatabaseInteractor {
    enum DatabaseError: Error {
        case unsupportedIdType
        case notFound
        case objectWasInvalidated
    }
}

internal extension Realm {
    internal func doWriteTransaction(file: String = #file, line: Int = #line, function: String = #function, _ block: (() throws -> Void)) {
        // Work around the crash that occurs when two write transactions are called in the same runloop.
        refresh()
        precondition(!isInWriteTransaction, "realm.write() was called when isInWriteTransaction == true")
        do {
            try self.write(block)
        } catch let error as NSError {
            let errorString = "Realm write error in \(function) (\(file) line \(line)): \(error)"
            print(errorString)
        }
    }
}

extension RealmDatabaseInteractor {
    
    fileprivate func writeOperation(operation: @escaping (Realm) -> Void,
                                    onError: (Error) -> Void,
                                    onCompleted: (() -> Void)? = nil) {
        do {
            let realm = try Realm(configuration: RealmDatabaseInteractor.config(databaseSchemaVersion, migrationBlock))
            if realm.isInWriteTransaction {
                operation(realm)
                try realm.commitWrite()
            } else {
                realm.doWriteTransaction {
                    operation(realm)
                    try realm.commitWrite()
                }
            }
            onCompleted?()
        } catch {
            print("FATAL: Error while interacting with Realm!\n\(error)")
            onError(error)
        }
    }
    
    fileprivate func readOperation(operation: (Realm) -> Void, onError: (Error) -> Void) {
        do {
            let realm = try Realm(configuration: RealmDatabaseInteractor.config(databaseSchemaVersion, migrationBlock))
            operation(realm)
        } catch {
            print("FATAL: Error while interacting with Realm!\n\(error)")
            onError(error)
        }
    }
}

extension RealmDatabaseInteractor: DatabaseStorageOne {
    public func add<T: DatabaseObject>(_ object: T) -> Observable<T> {
        return Observable<T>.create { observable in
            self.writeOperation(operation: { realm in
                realm.add(object, update: object.objectSchema.primaryKeyProperty != nil)
            }, onError: { (error) in
                observable.onError(error)
            }, onCompleted: {
                observable.onNext(object)
            })
            return Disposables.create()
        }
    }
    
    public func update<T>(_ object: T, writeBlock: @escaping (T) -> ()) -> Completable where T : DatabaseStorageOne.DatabaseObject {
        return Completable.create { completable in
            self.writeOperation(operation: { _ in
                writeBlock(object)
            }, onError: { (error) in
                completable(.error(error))
            }, onCompleted: {
                completable(.completed)
            })
            return Disposables.create()
        }
    }
    
    public func get<T: DatabaseObject, U>(id: U, type: T.Type) -> Single<T> {
        return Single<T>.create { single in
            guard id is String || id is Int || id is Bool || id is NSDate else {
                single(SingleEvent.error(RealmDatabaseInteractor.DatabaseError.unsupportedIdType))
                return Disposables.create()
            }
            self.readOperation(operation: { (realm) in
                if let object = realm.object(ofType: T.self, forPrimaryKey: id) {
                    single(SingleEvent.success(object))
                } else {
                    single(SingleEvent.error(RealmDatabaseInteractor.DatabaseError.notFound))
                }
            }, onError: { (error) in
                single(SingleEvent.error(error))
            })
            return Disposables.create()
        }
    }
    
    public func get<T: DatabaseObject, U>(id: U, type: T.Type) -> Observable<T> {
        return Observable<T>.create { observable in
            guard id is String || id is Int || id is Bool || id is NSDate else {
                observable.onError(RealmDatabaseInteractor.DatabaseError.unsupportedIdType)
                return Disposables.create()
            }
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                if let object = realm.object(ofType: T.self, forPrimaryKey: id) {
                    observable.onNext(object)
                    token = object.observe({ (_) in
                        if object.isInvalidated {
                            observable.onError(DatabaseError.objectWasInvalidated)
                        } else {
                            observable.onNext(object)
                        }
                    })
                } else {
                    observable.onError((RealmDatabaseInteractor.DatabaseError.notFound))
                }
            }, onError: { (error) in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        }
    }
    
    public func getFirst<T>(_ type: T.Type) -> Observable<T> where T: DatabaseObject {
        return Observable<T>.create { observable in
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                let objects = realm.objects(type)
                if let object = objects.first {
                    token = object.observe({ (_) in
                        if object.isInvalidated {
                            observable.onError(DatabaseError.objectWasInvalidated)
                        } else {
                            observable.onNext(object)
                        }
                    })
                    observable.onNext(object)
                } else {
                    token = objects.observe { changeset in
                        switch changeset {
                        case.initial:
                            break
                        case .update(let values, _, _, _):
                            if let first = values.first {
                                observable.onNext(first)
                            }
                        case.error(let error):
                            observable.onError(error)
                        }
                    }
                }
            }, onError: { (error) in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        }
    }
    
    public func delete<T: DatabaseObject>(_ object: T) -> Completable {
        return Completable.create { completable in
            self.writeOperation(operation: { _ in
                object.delete()
                completable(.completed)
            }, onError: { (error) in
                if error == DatabaseError.notFound {
                    completable(.completed)
                } else {
                    completable(.error(error))
                }
            })
            return Disposables.create()
        }
    }
}

extension RealmDatabaseInteractor: DatabaseStorageMany {
    public func addAll<T: Sequence>(_ objects: T) -> Observable<T> where T.Iterator.Element: DatabaseObject {
        return Observable<T>.create { observable in
            self.writeOperation(operation: { realm in
                var shouldUpdate = false
                if let object = objects.first(where: { (_) -> Bool in
                    return true
                }) {
                    shouldUpdate = object.objectSchema.primaryKeyProperty != nil
                }
                realm.add(objects, update: shouldUpdate)
            }, onError: { (error) in
                observable.onError(error)
            }, onCompleted: {
                observable.onNext(objects)
            })
            return Disposables.create()
        }
    }
    
    public func getAll<T>(_ type: T.Type) -> Observable<Results<T>> where T: DatabaseObject {
        return Observable<Results<T>>.create { observable in
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                let objects = realm.objects(T.self)
                observable.onNext(objects)
                token = objects.observe { changeset in
                    switch changeset {
                    case.initial:
                        break
                    case .update(let values, _, _, _):
                        observable.onNext(values)
                    case.error(let error):
                        observable.onError(error)
                    }
                }
            }, onError: { (error) in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        }
    }
    
    public func getWithPredicate<T>(predicate: Observable<NSPredicate?>,
                                    type: T.Type) -> Observable<Results<T>> where T : DatabaseObject {
        return Observable<Results<T>>.create { observable in
            var dispose: Disposable?
            let obs = predicate.subscribe(onNext: { (newPredicate) in
                dispose?.dispose()
                dispose = self.getWithPredicate(nsPredicate: newPredicate, type: type)
                    .subscribe(onNext: { (results) in
                        observable.onNext(results)
                    },
                               onError: observable.onError)
            },
                                          onError: observable.onError,
                                          onCompleted: observable.onCompleted)
            
            return Disposables.create {
                obs.dispose()
            }
        }
    }
    
    private func getWithPredicate<T>(nsPredicate: NSPredicate?,
                                     type: T.Type) -> Observable<Results<T>> where T : Object {
        return Observable<Results<T>>.create { observable in
            var token: NotificationToken?
            self.readOperation(operation: { realm in
                var objects = realm.objects(T.self)
                if let predicate = nsPredicate {
                    objects = objects.filter(predicate)
                }
                observable.onNext(objects)
                token = objects.observe { changeset in
                    switch changeset {
                    case.initial:
                        break
                    case .update(let values, _, _, _):
                        observable.onNext(values)
                    case.error(let error):
                        observable.onError(error)
                    }
                }
            }, onError: { (error) in
                observable.onError(error)
            })
            return Disposables.create {
                token?.invalidate()
            }
        }
    }
    
    public func updateAll<T>(_ objects: T,
                             writeBlock: @escaping (T) -> ())
        -> Completable where T: Sequence, T.Element: DatabaseStorageOne.DatabaseObject {
            return Completable.create { completable in
                self.writeOperation(operation: { _ in
                    writeBlock(objects)
                }, onError: { (error) in
                    completable(.error(error))
                }, onCompleted: {
                    completable(.completed)
                })
                return Disposables.create()
            }
    }
    
    public func updateAll<T,U>(_ objects: T,
                               writeBlockForEachElement: @escaping (U) -> ())
        -> Completable where T: Sequence, U == T.Element, U: DatabaseStorageOne.DatabaseObject {
            return Completable.create { completable in
                self.writeOperation(operation: { _ in
                    objects.forEach { writeBlockForEachElement($0) }
                }, onError: { (error) in
                    completable(.error(error))
                }, onCompleted: {
                    completable(.completed)
                })
                return Disposables.create()
            }
    }
    
    public func deleteAll<T>(_ objects: T.Type) -> Completable where T : DatabaseStorageMany.DatabaseObject {
        return Completable.create { completable in
            self.writeOperation(operation: { realm in
                let realmObjects = realm.objects(objects)
                realmObjects.forEach({ (object) in
                    object.delete()
                })
            }, onError: { (error) in
                if error == DatabaseError.notFound {
                    completable(.completed)
                } else {
                    completable(.error(error))
                }
            }, onCompleted: {
                completable(.completed)
            })
            return Disposables.create()
        }
    }
    
    public func deleteAll<T: Sequence>(_ objects: T) -> Completable where T.Iterator.Element: DatabaseObject {
        return Completable.create { completable in
            self.writeOperation(operation: { _ in
                objects.forEach({ (object) in
                    object.delete()
                })
            }, onError: { (error) in
                if error == DatabaseError.notFound {
                    completable(.completed)
                } else {
                    completable(.error(error))
                }
            }, onCompleted: {
                completable(.completed)
            })
            return Disposables.create()
        }
    }
    
    public func deleteCurrentAndInsertAll<T: Sequence>(_ objects: T)
        -> Observable<T> where T.Iterator.Element: DatabaseObject {
            return Observable<T>.create { observable in
                self.writeOperation(operation: { realm in
                    let realmObjects = realm.objects(T.Element.self)
                    realmObjects.forEach({ (object) in
                        object.delete()
                    })
                    var shouldUpdate = false
                    if let object = objects.first(where: { (_) -> Bool in
                        return true
                    }) {
                        shouldUpdate = object.objectSchema.primaryKeyProperty != nil
                    }
                    realm.add(objects, update: shouldUpdate)
                }, onError: { (error) in
                    observable.onError(error)
                }, onCompleted: {
                    observable.onNext(objects)
                })
                return Disposables.create()
            }
    }
}
