//
//  JWTInteractor.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 5/22/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

enum JWTInteractorError: Error {
    case failedToParseFromStorage
}

/// JWTInteractor
///
/// An interactor providing all necessary features for JWT to properly work
/// It's able to persist recieved tokens from a login request. Sign any request with these tokens.
///
/// Example usage
///
///    1. Init the dependencies of the interactor
///       ```
///         let jwt = JWTInteractor()
///         jwt.dependencies = HasSecureStorage
///       ```
///
///    2. Init the tokens -> create a login request (this is not included in this interactor
///       as the login response does not conform to OAuth2 specifications)
///    ```
///         jwt.saveTokens(jwtToken: "jwtToken")
///    ```
///
///   3. Use the instance of the interactor to sign requests
///   ```
///        jwt.signRequest(unsignedRequest, callback: { (request) in
///            if let jwtSignedRequest = request {
///                done(.success(jwtSignedRequest))
///            } else {
///                done(.failure(MoyaError.requestMapping("Error signing the request with JWT")))
///            }
///       })
///
public class JWTInteractor {
    typealias Dependencies = HasSecureStorage

    fileprivate let useraccount = "JWT-useraccount"
    var dependencies: Dependencies!

    init() {
        // Init without any parameters
    }

}

// MARK: - Public
extension JWTInteractor {
    /// Persist tokens for JWT to secured storage
    ///
    /// - Parameter jwtToken:
    func saveTokens(jwtToken: String) {
        dependencies.securedStorage.save(account: useraccount, data: [
            "jwt.token": jwtToken
            ])
    }

    /// Sign an url request with JWT access token
    ///
    /// - Parameters:
    ///   - request: request to be signed
    ///   - callback: callback with signed request
    func signRequest(_ request: URLRequest, callback: @escaping (URLRequest?) -> Void) {
        let request = signRequest(request)
        callback(request)
    }

    /// Delete the persisted token
    func deleteToken() {
        dependencies.securedStorage.delete(account: useraccount)
    }

}

// MARK: - Private
extension JWTInteractor {
    fileprivate func signRequest(_ request: URLRequest) -> URLRequest? {
        var request = request
        do {
            try request.addValue("\(getToken())", forHTTPHeaderField: "jwt")
        } catch {
            return nil
        }
        return request
    }

    fileprivate func getToken() throws -> String {
        let data = dependencies.securedStorage.loadData(account: useraccount)
        if let jwtToken = data["jwt.token"] as? String {
            return jwtToken

        }
        throw OAuthInteractorError.failedToParseFromStorage
    }
}
