//
//  HasJWT.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 7/24/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

/// HasJWT
///
/// A protocol that provides an access to the JWT signing interactor
public protocol HasJWT {
    var jwt: JWTInteractor { get }
}
