//
//  OAuthInteractor.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 5/22/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

private extension Date {
    var isInPast: Bool {
        let now = Date()
        return self.compare(now) == ComparisonResult.orderedAscending
    }
}

/// OAuthInteractorError
///
/// - failedToParseFromStorage: Failed to read the token from secured storage
/// - expiredToken: The token is expired we need a new one
enum OAuthInteractorError: Error {
    case failedToParseFromStorage
    case expiredToken
}

/// HasOAuth
///
/// A protocol that provides an access to the OAuth signing interactor
public protocol HasOAuth {
    var oauth: OAuthInteractor { get }
}

/// OAuthInteractor
///
/// An interactor providing all necessary features for OAuth2 to properly work
/// It's able to persist recieved tokens from a login request. Sign any request with these tokens 
/// and verify if the `accessToken` is expired or not. When the token is expired it will automaically try
/// to renew it.
///
/// Example usage
/// 
///    1. Init the dependencies of the interactor
///       ```
///         let oAuth = OAuthInteractor()
///         oAuth.dependencies = HasSecureStorage & HasNetworking
///       ```
///
///    2. Init the tokens -> create a login request (this is not included in this interactor
///       as the login response does not conform to OAuth2 specifications)
///    ``` 
///         oAuth.saveTokens(refreshToken: "refreshToken",
///                          accessToken: "token",
///                          expiresIn: 3600,
///                          scope: "security")
///    ```
///
///   3. Use the instance of the interactor to sign requests
///   ```
///        oAuth.signRequest(unsignedRequest, callback: { (request) in
///            if let oAuthSignedRequest = request {
///                done(.success(oAuthSignedRequest))
///            } else {
///                done(.failure(MoyaError.requestMapping("Error signing the request with OAuth")))
///            }
///       })
///
public class OAuthInteractor {
    typealias Dependencies = HasSecureStorage & HasNetworking

    fileprivate let useraccount = "OAUTH-useraccount"
    var dependencies: Dependencies!

    init() {
        // Init without any parameters
    }
}

// MARK: - Public
extension OAuthInteractor {
    /// Persist tokens for OAuth to secured storage
    ///
    /// - Parameters:
    ///   - refreshToken:
    ///   - accessToken:
    ///   - expiresIn: seconds in which the token will expire
    ///   - scope:
    func saveTokens(refreshToken: String,
                    accessToken: String,
                    expiresIn: Int,
                    scope: String) {
        saveTokens(refreshToken: refreshToken,
                   accessToken: accessToken,
                   expiresIn: Date().addingTimeInterval(Double(expiresIn)),
                   scope: scope)
    }

    /// Persist tokens for OAuth to secured storage
    ///
    /// - Parameters:
    ///   - refreshToken:
    ///   - accessToken:
    ///   - expiresIn: Date when the token will expire
    ///   - scope:
    func saveTokens(refreshToken: String,
                    accessToken: String,
                    expiresIn: Date,
                    scope: String) {
        dependencies.securedStorage.save(account: useraccount, data: [
            "oauth.access_token": accessToken,
            "oauth.expires_in": expiresIn,
            "oauth.scope": scope,
            "oauth.refresh_token": refreshToken
            ])
    }

    /// Sign an url request with OAuth access token
    ///
    /// - Remark: This could take a while as the OAuth will perform a token refresh automatically 
    ///   if access token is expired
    ///
    /// - Parameters:
    ///   - request: request to be signed
    ///   - callback: callback with signed request
    func signRequest(_ request: URLRequest, callback: @escaping (URLRequest?) -> Void) {
        if isTokenValid() {
            let request = signRequest(request)
            callback(request)
            return
        }
        if let token = try? getRefreshToken() {
            _ = dependencies.networking.`public`.provider.request(.refreshToken(token: token.refreshToken))
                .mapJSON()
                .subscribe(onNext: { [weak self] (response: RefreshTokenEntity) in
                    if let stronSelf = self {
                        stronSelf.saveTokens(refreshToken: response.refreshToken,
                                             accessToken: response.accessToken,
                                             expiresIn: response.expiresIn,
                                             scope: response.accessToken)
                        let request = stronSelf.signRequest(request)
                        callback(request)
                    } else {
                        callback(nil)
                    }
                    }, onError: { (_) in
                        callback(nil)
                })
            return
        }
        callback(request)
    }

    /// Delete the persisted token
    func deleteToken() {
        dependencies.securedStorage.delete(account: useraccount)
    }
}

// MARK: - Private
extension OAuthInteractor {
    fileprivate func getToken() throws -> String {
        let data = dependencies.securedStorage.loadData(account: useraccount)
        if let accessToken = data["oauth.access_token"] as? String {
            return (accessToken)
        }
        throw OAuthInteractorError.failedToParseFromStorage
    }

    fileprivate func getRefreshToken() throws -> (refreshToken: String, scope: String) {
        let data = dependencies.securedStorage.loadData(account: useraccount)
        if let refreshToken = data["oauth.refresh_token"] as? String,
            let scope = data["oauth.scope"] as? String {
            return (refreshToken, scope)
        }
        throw OAuthInteractorError.failedToParseFromStorage
    }

    fileprivate func isTokenValid() -> Bool {
        let data = dependencies.securedStorage.loadData(account: useraccount)
        if let expiresIn = data["oauth.expires_in"] as? Date {
            return !expiresIn.isInPast
        }
        return false
    }

    fileprivate func isRefreshTokenValid() -> Bool {
        let data = dependencies.securedStorage.loadData(account: useraccount)
        if data["oauth.refresh_token"] as? String != nil {
            return true
        }
        return false
    }

    fileprivate func signRequest(_ request: URLRequest) -> URLRequest? {
        var request = request
        do {
            try request.addValue("Bearer \(getToken())", forHTTPHeaderField: "Authorization")
        } catch {
            return nil
        }
        return request
    }
}
