//
//  AppDelegateInteractor.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 4/11/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

public class AppDelegateInteractor {
    public static let sharedInstance = AppDelegateInteractor()

    public static func  setApplicationWillEnterForegroundCallback(callback: @escaping () -> Void) {
        AppDelegateInteractor.sharedInstance.applicationWillEnterForegroundCallback = {
            callback()
            AppDelegateInteractor.sharedInstance.applicationWillEnterForegroundCallback = nil
        }
    }

    public private(set) var applicationWillEnterForegroundCallback: (() -> Void)?
}
