//
//  HasSecureStorage.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 7/24/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation

/// HasSecureStorage
///
/// A protocol that provides an access to the native secured(encrypted) storage
public protocol HasSecureStorage {
    var securedStorage: SecureStorage { get }
}
