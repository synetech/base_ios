//
//  SecureStorageInteractor.swift
//  SYNETECH
//
//  Created by Daniel Rutkovsky on 5/19/17.
//  Copyright © 2018 SYNETECH. All rights reserved.
//

import Foundation
import Locksmith

/// A contract that a secured storage interactor has
/// It's designed to store data values for given account in a secured location (Keychain)
public protocol SecureStorage {
    /// Save method that saves provided map of data to the secured location
    ///
    /// - Remark: When there are some data for this account it will perform update automatically
    /// - Parameters:
    ///   - account: Account for which the data are stored
    ///   - data: Data to be stored
    func save(account: String, data: [String: Any])

    /// Update method that saves provided map of data to the secured location
    ///
    /// - Parameters:
    ///   - account: Account for which the data are stored
    ///   - data: Data to be stored
    func update(account: String, data: [String: Any])

    /// Get method that loads data from the secured location for given account
    ///
    /// - Parameter account: Account for which the data are stored
    /// - Returns: Retrieved data
    func loadData(account: String) -> [String: Any]

    /// Delete method that removes all data from the secured location for given account
    ///
    /// - Parameter account: Account for which the data are stored
    func delete(account: String)
}

public struct SecureStorageInteractor: SecureStorage {

    public init() {
    }

    public func save(account: String, data: [String: Any]) {
        do {
            try Locksmith.saveData(data: data, forUserAccount: account)
        } catch LocksmithError.duplicate {
            update(account: account, data: data)
        } catch {
            fatalError("Failed to write into the secured storage. \(error.localizedDescription)")
        }
    }

    public func update(account: String, data: [String: Any]) {
        do {
            try Locksmith.updateData(data: data, forUserAccount: account)
        } catch {
            fatalError("Failed to write into the secured storage. \(error.localizedDescription)")
        }
    }

    public func loadData(account: String) -> [String: Any] {
        return Locksmith.loadDataForUserAccount(userAccount: account) ?? [:]
    }

    public func delete(account: String) {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: account)
        } catch LocksmithError.notFound {
            return
        } catch {
            fatalError("Failed to write into the secured storage. \(error.localizedDescription)")
        }
    }
}
