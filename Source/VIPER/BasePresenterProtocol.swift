//
//  BasePresenterProtocol.swift
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/15/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import Foundation

/// BasePresenterProtocol
///
/// A protocol that ensures that every presenter can react on view when it's loaded
public protocol BasePresenterProtocol {
    /// Notification from the view that it has been loaded / displayed
    /// Presenter should start presenting content on the view at this point.
    func loadContent()
}
