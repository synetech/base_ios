//
//  BaseViewProtocol.swift
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/15/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import UIKit

/// BaseViewProtocol
///
/// A protocol that ensures that the view controller can display loading dialogs in a unified way.
///
public protocol LoadingViewProtocol {

    /// Show loading dialog with a optional message
    ///
    /// - Parameter message: (optional) message to be displayed along with the loading
    func showLoading(message: String?)

    /// Hide loading dialog if present
    func hideLoading()
}

/// ErrorViewProtocol
///
/// A protocol that ensures that the view controller can display error messages in a unified way.
///
public protocol ErrorViewProtocol {
    /// Display error message with basic _OK_ action.
    /// This message is shown to the user, so make it user readable.
    ///
    /// - Parameters:
    ///   - title: Title of the message
    ///   - message: Short desctiption of the error
    ///   - actions: A list of actions (UIAlertAction) that could be done
    func showError(title: String, message: String, actions: [UIAlertAction])
}
