//
//  BaseViewController.swift
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/15/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import UIKit

/// BaseViewController
///
/// A base implemnetation of a ViewController for (B)VIPER architecture used among our projects.
/// This controller is presented by router and it owns its presenter, therefore it has to be instatiated with 
/// a instance of the presenter.
///
open class BaseViewController<P: BasePresenterProtocol>: UIViewController {

    // MARK: - Variables
    internal var presenter: P

    // MARK: - Init

    /// The designated initializer
    ///
    /// - Parameter presenter: A presenter instance that will controll this view
    public init(presenter: P) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Prepare Layout

    /// This function should contain all code that setups / styles / prepares views in this View Controller
    open func setupViews() {
    }

    /// This function should contain all layout code for this View Controllers
    open func layoutViews() {
        fatalError("Override this method in child class")
    }

    // MARK: - Life cycle
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }

    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.layoutViews()
    }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.loadContent()
    }
}
