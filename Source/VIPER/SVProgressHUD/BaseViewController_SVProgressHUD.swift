//
//  BaseViewController_SVProgressHUD.swift
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/23/17.
//  Copyright © 2017 SYNETECH. All rights reserved.
//

import SVProgressHUD
import UIKit

/// A default implementation of `LoadingViewProtocol` for every UIViewController
/// This implementation can be overriden if needed
extension LoadingViewProtocol where Self: UIViewController {
    public func showLoading(message: String?) {
        if let message = message {
            SVProgressHUD.show(withStatus: message)
        } else {
            SVProgressHUD.show()
        }
    }

    public func hideLoading() {
        SVProgressHUD.dismiss()
    }
}

extension ErrorViewProtocol where Self: UIViewController {

    public func showError(title: String, message: String, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { (action) in
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
    }
}
