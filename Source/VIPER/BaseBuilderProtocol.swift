//
//  BaseBuilder.swift
//  BaseProject
//
//  Created by Daniel Rutkovsky on 3/15/17.
//  Copyright © 2017 Daniel Rutkovsky. All rights reserved.
//

import UIKit

/// BaseBuilder
///
/// A protocol that provides a basic contract between a router and a new screen that should be presented.
/// This contract ensures that each screen is built by a builder and `UIViewController` is returned.
/// Therefore router does not know about architecture that is used by that screen.
///
public protocol BaseBuilder {
    associatedtype RouterProtocol

    /// A basic function of a builder is to build a `UIViewController` and return it.
    ///
    /// - Parameter router: A router that will listen to _finish_ and _cancel_ eventes. And will emeit `onCancel()` or
    ///   `onFinish()` event. Because the screen has to notify the router that it's flow has ended or was canceled.
    /// - Returns: A `UIViewController` that should be presented.
    ///
    static func buildViewController(router: RouterProtocol) -> UIViewController?
}

public protocol BaseBuilderWithDependencies {
    associatedtype RouterProtocol
    associatedtype Dependencies
    /// A basic function of a builder is to build a `UIViewController` and return it.
    ///
    /// - Parameter router: A router that will listen to _finish_ and _cancel_ eventes. And will emeit `onCancel()` or
    ///   `onFinish()` event. Because the screen has to notify the router that it's flow has ended or was canceled.
    /// - Returns: A `UIViewController` that should be presented.
    ///
    static func buildViewController(router: RouterProtocol, dependencies: Dependencies) -> UIViewController?
}

public protocol BaseBuilderWithParams {
    associatedtype RouterProtocol
    /// A basic function of a builder is to build a `UIViewController` and return it.
    ///
    /// - Parameter router: A router that will listen to _finish_ and _cancel_ eventes. And will emeit `onCancel()` or
    ///   `onFinish()` event. Because the screen has to notify the router that it's flow has ended or was canceled.
    /// - Returns: A `UIViewController` that should be presented.
    ///
    static func buildViewController(router: RouterProtocol,
                                    params: [String: Any]) -> UIViewController?
}

public protocol BaseBuilderWithDependenciesAndParams {
    associatedtype RouterProtocol
    associatedtype Dependencies
    /// A basic function of a builder is to build a `UIViewController` and return it.
    ///
    /// - Parameter router: A router that will listen to _finish_ and _cancel_ eventes. And will emeit `onCancel()` or
    ///   `onFinish()` event. Because the screen has to notify the router that it's flow has ended or was canceled.
    /// - Returns: A `UIViewController` that should be presented.
    ///
    static func buildViewController(router: RouterProtocol,
                                    dependencies: Dependencies,
                                    params: [String: Any]) -> UIViewController?
}
