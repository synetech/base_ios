//
//  HasDependecyInjection.swift
//  DependecyInjectionProtocol
//
//  Created by Daniel Rutkovsky on 7/18/18.
//  Copyright © 2018 Synetech. All rights reserved.
//

import Foundation

public protocol HasDependecyInjection {
    associatedtype Dependecies
    init(dependecies: Dependecies)
}
