#
#  Be sure to run `pod spec lint SYNBaseProject.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "SYNBase"
  s.version      = "1.2.8"
  s.summary      = "A base project that probivies a unfied functionality to all our projects"
  s.description  = <<-DESC
                    This project provides a base functions, structures and approaches that all our new projects depend on.
                    DESC
  s.homepage     = "https://bitbucket.org/synetech/base_ios"
  s.license      = "MIT"
  s.author       = { "Daniel Rutkovsky" => "daniel.rutkovsky@synetech.cz" }

  s.source       = { :git => "git@bitbucket.org:synetech/base_ios.git", :tag => "#{s.version}" }
  # s.source_files  = "Source",

  s.requires_arc = true
  s.platform     = :ios, "9.0"

  s.subspec 'DependencyInjection' do |ss|
    ss.source_files = 'Source/DependencyInjection/**/*.swift'
  end

  s.subspec 'Routing' do |ss|
    ss.source_files = 'Source/Routing/**/*.swift'
  end

  s.subspec 'VIPER' do |ss|
    ss.source_files = 'Source/VIPER/**/*.swift'
    ss.exclude_files = 'Source/VIPER/SVProgressHUD'

    ss.subspec 'SVProgressHUD' do |sss|
      sss.source_files = 'Source/VIPER/**/*.swift'
      sss.dependency 'SVProgressHUD', '>= 2.1'
    end

  end

  s.subspec 'Loading' do |ss|
    ss.source_files = 'Source/Loading.swift'
    ss.dependency 'SYNBase/VIPER'
    ss.dependency 'RxSwift', '>= 4'
  end

  s.subspec 'Scalable' do |ss|
    ss.source_files = 'Source/Scalable/**/*.swift'
    ss.dependency "Device", '>= 3.1'
  end

  s.subspec 'Extensions' do |ss|
    ss.subspec 'ObjectMapper' do |sss|
      sss.source_files = 'Source/Extensions/ObjectMapper/*.swift'
      sss.dependency 'ObjectMapper', '>= 3'
      sss.dependency 'Moya'
      sss.dependency 'RxCocoa', '>= 4'
      sss.dependency 'RxSwift', '>= 4'
    end

    ss.subspec 'Rx' do |sss|
      sss.source_files = 'Source/Extensions/Rx/*.swift'
      sss.dependency 'RxCocoa', '>= 4'
      sss.dependency 'RxSwift', '>= 4'
    end

    ss.subspec 'Other' do |sss|
      sss.source_files = 'Source/Extensions/Other/*.swift'
    end
  end

  s.subspec 'Interactors' do |ss|
    ss.subspec 'AppDelegate' do |sss|
    sss.source_files = 'Source/Interactors/AppDelegate/**/*.swift'
  end

  ss.subspec 'DatabaseStorage' do |sss|
    sss.source_files = 'Source/Interactors/DatabaseStorage/**/*.swift'
    sss.dependency 'RealmSwift', '>= 3'
    sss.dependency 'RxSwift', '>= 4'
  end

  ss.subspec 'KeyValueStorage' do |sss|
    sss.source_files = 'Source/Interactors/KeyValueStorage/**/*.swift'
  end

  ss.subspec 'SecuredStorage' do |sss|
    sss.source_files = 'Source/Interactors/SecuredStorage/**/*.swift'
    sss.dependency 'Locksmith', '>= 3.0'
  end

  # ss.subspec 'OAuth' do |sss|
  #   sss.source_files = 'Source/Interactors/OAuth/**/*.swift'
  # end

  # ss.subspec 'JWT' do |sss|
  #   sss.source_files = 'Source/Interactors/JWT/**/*.swift'
  # end

  end

end
