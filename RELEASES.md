# RELEASES

## [1.2.0]
* Support for swift 4.2
* Split ObjectMapper extensions into a separate subpod

## 1.1.0
* Migrated to __Swift 4__
* _Dependecy:_ Realm updated. __Minimal supported__ version is version 4.0.

## 1.0.15
* _Dependecy:_ Realm updated. __Minimal supported__ version is version 3.0.

## 1.0.14
* _Dependecy:_ Realm - added support for migration block for database storage

## 1.0.13
* ⚠️ Not documented 